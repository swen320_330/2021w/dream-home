# 1.	Development Requirements
## 1.1	Language Requirements
System should be developed in TypeScript.  
Rationale: Both teammates have experiece in TypeScript (less of a requirement, more of a preference)   
## 1.2	Platform Requirements
1: the system is a web-based system  
2: the system uses nginx webserver, NodeJS compute, ReactJS/HTML5 UI, and MySQL database  
# 2.	Functional Requirements
Capability: C-1: Owners and clients can login/out (must be registered)   
Capability: CU-1: Unrented properties are listed on the website  
Capability: CU-2: Sort and filter properties by distance, rent, #rooms, type   
Capability: CS-1: Register clients who can look at properties and owners who can add properties  
Capability: CS-2: Owners can add and edit properties and their properties   
Capability: CS-3: Clients and owners can sort and filter properties like `CU-2` as well as by time unrented   
Capability: CS-4: Owners can add leases (we are implementing this)  
The [spec](https://drive.google.com/file/d/1fJRBHFcvZtvEoreb9aInJGSEC16T2hQU/view?usp=sharing) is incorporated by reference 
# 3.	Interface Requirements
## 3.1	User Interface Requirement
Interface Requirement: IR-1: Map of property locations is available
Rationale: Clients can see where properties are located in relation to services
## 3.2	Communication Interface Requirement
IR-3: Communication between client and server is via REST/JSON, Spec is in `types.ts`
# 4.	Non-functional Requirements
## 4.1	Availability - 3.5 nines
## 4.2	Usability 
## 4.3	Performance - &lt;1 sec response time
## 4.4	Security 

# Deployment Guide
To be written later  
(Probably `git pull; docker build; docker-compose up`)

