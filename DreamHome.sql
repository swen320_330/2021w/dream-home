-- SQLite syntax
-- create schema "DreamHome";
create table "DreamHome.Branch" (
    "BranchNum" integer primary key not null,
    "Manager" integer not null references "DreamHome.Staff"("ID"),
    "Address.St" varchar(50) not null,
    "Address.city" varchar(50) not null,
    "Address.postcode" char(8) not null,
    "Telephone.prim" char(12) not null unique,
    "Telephone.ext"varchar(25) not null,
    "Manager.Date"char(10) not null,
    "Manager.Bonus"real check (not ("Manager.Bonus"<0)),
    unique ("Address.St","Address.City") -- no 2 branches at same phys. loc
);
create table "DreamHome.Staff" (
    "ID" integer primary key not null,
    "Role" varchar(20) not null,
    "Supervisor" integer references "DreamHome.Staff"("ID"),
    "Branch" integer references "DreamHome.Staff"("ID"),
    "Name" varchar(100) not null,
    "Address.St" varchar(50) not null,
    "Address.city" varchar(50) not null,
    "Address.postcode" char(8) not null,
    "Salary" real not null check("Salary">0)
);
create table "DreamHome.Property" (
    "ID" varchar(12) primary key not null,
    "Address.St" varchar(50) not null,
    "Address.city" varchar(50) not null,
    "Address.postcode" char(8) not null,
    "Type" char(1) not null check ("Type" in ("H","F")),
    "numRooms" integer not null check ("numRooms">1),
    "rent" real not null check ("rent">0),
    "owner" integer not null references "DreamHome.Owner"("ID"),
    "manager" integer not null references "DreamHome.Staff",
    unique ("Address.St","Address.City") -- no 2 properties at same phys. loc
);
create table "DreamHome.Owner"(
    "ID" integer primary key not null,
    "Name" varchar(100) not null,
    "Telephone" char(12) not null,
    "Type" char(1) not null check ("Type" in ("P","B")),
    "Email" varchar(255) not null,
    "PassHash" blob not null,
    "Biz.Type" varchar(100),
    "Biz.Contact" varchar(100)
    unique ("Email") -- only 1 acct per email addr?
);
create table "DreamHome.Client"(
    "ID" integer primary key not null,
    "Name" varchar(100) not null,
    "Telephone" char(12) not null,
    "Email" varchar(255) not null,
    "Pref" char(1) not null check ("Pref" in ("H","F")),
    "MaxRent" real not null check ("MaxRent">0),
    "Branch" integer references "DreamHome.Branch"("BranchNum"),
    "Staff" integer references "DreamHome.Staff"("ID"),
    "SignDate" char(10) not null,
    unique ("Email") -- only 1 acct per email addr?
);
create table "DreamHome.Lease"(
    "ID" integer not null primary key,
    "Deposit" boolean not null,
    "Property" integer not null references "DreamHome.Property"("ID"),
    "Client" integer not null references "DreamHome.Client"("ID"),
    "StartDate" char(10) not null,
    "Duration" integer not null,
    "Rent" real not null check("Rent">0),
    -- unique ("Property","Client","StartDate")
);
create table "DreamHome.Ad"(
    "Property" integer not null references "DreamHome.Property"("ID"),
    "Paper" integer not null references "DreamHome.Newspaper"("ID"),
    "Date" char(10) not null,
    "Cost" real not null check("Cost">0),
    primary key("Property","Paper","Date")
);
create table "DreamHome.Newspaper"(
    "ID" integer not null primary key,
    "Name" varchar(100) not null,
    "Contact" varchar(100) not null,
    "Address.St" varchar(50) not null,
    "Address.city" varchar(50) not null,
    "Address.postcode" char(8) not null,
    unique ("Address.St","Address.city")
);
create table "DreamHome.Viewing"(
    "Client" integer not null references "DreamHome.Client"("ID"),
    "Property" integer not null references "DreamHome.Property"("ID"),
    "Date" char(10) not null,
    "Comments" blob,
    primary key("Client","Property","Date")
);