import type { Express } from 'express'
import { search,byID } from './api/search'
import {addHome} from './api/addHome'
export function setApis(e: Express) {
    e.post('/search/homes', search)
    e.get('/home/ref/$id',byID)
    e.post('/home/new',addHome)
}