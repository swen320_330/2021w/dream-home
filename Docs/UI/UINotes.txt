City - Valid operators are equal, any of, or not any of.
Rent - Valid operators are LT, LTE, GT, GTE, Between.
Postcode - Valid operators are any of, is, not any of.
Bedrooms - Valid operators are LT, LTE, GT, GTE, Eq, Between, any of
Type - Either Flat or House
Street - Valid operators are is, any of, not any of.
Area - Same as Street
Dont care is valid for all parameters.