import { init } from './consts'
import { setApis } from './API'
import e = require('express')
async function main() {
    await init()
    let app = e()
    app.use(e.json())
    setApis(app);
	app.listen(8080);
}
main();