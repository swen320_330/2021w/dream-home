import * as mongodb from 'mongodb'
import type { Property } from './types'
let doneInit = false;
export async function init() {
    if (doneInit) return;
    doneInit = true;
    await dbinit()
}
async function dbinit() {
    conn = await mongodb.connect('mongodb://localhost:27017')
    db = conn.db('dream-home')
    properties = db.collection('properties')
}
export let conn: mongodb.MongoClient;
export let db: mongodb.Db;
export let properties: mongodb.Collection<Property>