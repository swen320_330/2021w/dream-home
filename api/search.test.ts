import {ba,be,props} from '../Test/PropertiesDB'
import {searchImpl} from '../api/search'
import { Predicate } from '../types'
beforeAll(ba)
beforeEach(be)
describe('Search Homes',()=>{
    test('finds all flats',async ()=>{
        let o = await searchImpl([{flat:{pred:Predicate.EQ,value1:true}}])
        expect(o).toHaveLength(5)
    })
})
