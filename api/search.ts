import type { RequestHandler } from 'express'
import { db, properties } from '../consts'
import type { Message, Property, SearchItem, SearchBody,} from '../types'
import { Predicate } from '../types'
import type { Condition, RootQuerySelector } from 'mongodb'

export let search: RequestHandler = (req, res) => {
    let m: Message = req.body
    if (m.tag !== 'search.req') {
        res.status(400).send("Invalid message type for API, expected 'search.req'").end()
        return
    }
    if (!(m.query instanceof Array))
    m.query = [m.query]
    let q = m.query
    try {
        searchImpl(m.query).then((v)=>{
            res.status(200).json(
                {tag:'search.result',query:q,results:v}
                ).end()
        })
    } catch (e) {
        res.status(400).send(`Bad format, expected ${e}`).end()
        return
    }

}

export async function searchImpl(q: SearchBody):Promise<Property[]> {
    let collector: RootQuerySelector<Property>[] = []
    for (let o of q) {
        let s: RootQuerySelector<Property> = {}
        for (let i in o) {
            //@ts-ignore
            s[i] = buildQuery(o[i])
        }
        collector.push(s)
    }
    let cur = properties.find({ $or: collector })
    let res = await cur.toArray()
    cur.close()
    return res
}

function buildQuery<T>(i: SearchItem<T>): Condition<unknown> {
    let v1 = i.value1
    let v2 = i.value2
    let add = i.addls
    switch (i.pred) {
        case Predicate.Any:
            {
                let arr
                arr = [v1]
                arr.push(...add || [])
                if (v2) arr.push(v2)
                return { $in: arr }
            }
        case Predicate.Between:
            if (!v2) throw 'second value for predicate Between'
            return { $lte: v1 > v2 ? v1 : v2, $gte: v1 < v2 ? v1 : v2 }
        case Predicate.EQ:
            return { $eq: v1 }
        case Predicate.GT:
            return { $gt: v1 }
        case Predicate.GTE:
            return { $gte: v1 }
        case Predicate.LT:
            return { $lt: v1 }
        case Predicate.LTE:
            return { $lte: v1 }
        case Predicate.NBetween:
            if (!v2) throw 'second value for predicate NBetween'
            return { $not: { $lte: v1 > v2 ? v1 : v2, $gte: v1 < v2 ? v1 : v2 } }
        case Predicate.NEQ:
            return { $ne: v1 }
        case Predicate.None: {
            let arr = [v1]
            arr.push(...add || [])
            if (v2) arr.push(v2)
            return { $nin: arr }
        }
        default:
            const _:never = i.pred 
            return _
    }
}

export let byID:RequestHandler = (req,res)=>{
    let ref = req.params['id']
    properties.findOne({ref:ref}).then(
        (r)=>res.status(200).json({tag:'details.resp',...r}).end(),
        (e)=>res.status(404).json(e).end()
    )
}