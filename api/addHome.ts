import type { RequestHandler } from 'express'
import { db, properties } from '../consts'
import type { Message, Property, SearchItem, SearchBody,} from '../types'
import { Predicate } from '../types'
import type { Condition, RootQuerySelector } from 'mongodb'
export let addHome:RequestHandler = (req,res)=>{
    let m: Message = req.body
    if (m.tag !== 'home.add') {
        res.status(400).send("Invalid message type for API, expected 'home.add'").end()
        return
    }
    properties.insertOne(m.content).then((r)=>res.status(200).json({tag:'home.ref',id:r.insertedId}).end(),(e)=>res.status(500).json(e).end())
}