export type Message = 
    | MessageTo
    | MessageFrom
    | UID
type MessageTo =
    | Search
    | Auth_login
    | Auth_logout
    | getByID
    | AddHome
type MessageFrom =
    | Auth_cookie
    | Details
    | SearchResults
export interface UID{
    tag:'ref.home'
    id:string
}
export interface Auth_login {
    tag: 'auth.login'
    user: string
    ident: string
}
export interface Auth_logout {
    tag: 'auth.logout'
}
export interface Search {
    tag: 'search.req'
    query: SearchBody
}
export type SearchBody = SIMapper<Property>[]
export interface Auth_cookie {
    tag: 'auth.cred'
    value: string
}
export interface Details extends Partial<Property> {
    tag: 'details.resp'
}
export interface getByID {
    tag: 'find.by.ID'
    ref: string
}
export interface SearchResults {
    tag: 'search.result'
    query: Search
    results: Partial<Property>[]
}
export interface AddHome{
    tag:'home.add'
    content:Property
}
export interface Property {
    city: string
    rent: number
    postcode: string
    bedrooms: number
    flat: boolean
    street: string
    area: string
    ref: string
}
type SIMapper<T> = { [K in keyof T]?: SearchItem<T[K]> };
export enum Predicate {
    LT, GT, LTE, GTE, EQ, NEQ, Between, NBetween, Any, None,
}
export type SearchItem<T> = { value1: T, pred: Predicate, value2?: T, addls?: T[] }