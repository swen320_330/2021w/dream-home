import {init,properties} from '../consts'
import type {Property} from '../types'
export async function ba():Promise<void>{
    await init();
}
export async function be():Promise<void>{
    await properties.deleteMany({})
    await properties.insertMany(props)
}
export const props:Property[] = [
    {area:'a',bedrooms:2,city:'l',flat:true,postcode:'wc1',ref:'wc1:1',rent:250,street:'e'},
    {area:'b',bedrooms:2,city:'o',flat:true,postcode:'w3',ref:'w3:1',rent:260,street:'e'},
    {area:'a',bedrooms:1,city:'n',flat:true,postcode:'nc2',ref:'nc2:1',rent:200,street:'e'},
    {area:'b',bedrooms:1,city:'d',flat:true,postcode:'so3w',ref:'so3w:1',rent:200,street:'e'},
    {area:'b',bedrooms:2,city:'o',flat:true,postcode:'99as',ref:'99as:1',rent:230,street:'e'},
    {area:'c',bedrooms:3,city:'n',flat:false,postcode:'dd90',ref:'dd90:1',rent:403,street:'e'},
    {area:'d',bedrooms:4,city:'b',flat:false,postcode:'dfs',ref:'dfs:1',rent:376,street:'e'},
    {area:'a',bedrooms:5,city:'r',flat:false,postcode:'ar3',ref:'ar3:1',rent:880,street:'e'},
    {area:'d',bedrooms:2,city:'i',flat:false,postcode:'wc1',ref:'wc1:2',rent:500,street:'e'},
    {area:'c',bedrooms:1,city:'d',flat:false,postcode:'wc1',ref:'wc1:3',rent:400,street:'e'},
    {area:'e',bedrooms:2,city:'g',flat:false,postcode:'w3',ref:'w3:2',rent:800,street:'e'},
]